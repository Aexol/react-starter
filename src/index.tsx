import React from "react";
import ReactDOM from "react-dom";
import styled from "@emotion/styled";
const logo = require("./logo.png")

const AppContainer = styled.div`
  margin: 0;
  padding: 0;
  text-align: center;
  display: flex;
  flex-direction: column;
`;

const Logo = styled.img`
  margin: 0 auto;
`;

const App: React.FC = () => {
  return (
    <AppContainer>
      <h2 className="Title">React Starter</h2>
      <Logo src={logo} />
    </AppContainer>
  );
};

ReactDOM.render(<App />, document.getElementById("root"));
