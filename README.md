# React Starter 

Simple react starter with typescript and styled components
## Installation


```bash
npm i
```

## Usage

```bash
npm run start
```


## License
[MIT]
